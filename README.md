# backup-tools

Backups-Tools is a specialized toolchain to create backups in container capable environment. The container image is build around the following major tools:

- [restic](https://github.com/restic/restic)
- [resticprofile](https://github.com/creativeprojects/resticprofile)
- [rclone](https://github.com/rclone/rclone)

Some extra packages are build into the container:

- mariadb-client
- postgresql-client
- mongotools
- zabbix-utils
